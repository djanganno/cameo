#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


celery -A cameolake.taskapp beat -l INFO
