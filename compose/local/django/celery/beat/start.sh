#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

echo '\n\n *************************  celery/start.sh  **********************'

rm -f './celerybeat.pid'
celery -A cameolake.taskapp beat -l INFO
