import random
from fabric.contrib.files import append, exists
from fabric.api import cd, env, local, run

REPO_URL = 'https://gitlab.com/djanganno/cameo'

def deploy():
    # site_folder = f'/home/{env.user}/sites/{env.host}'
    site_folder = f'/home/ubuntu/cameo'
    run(f'mkdir -p {site_folder}')
    with cd(site_folder):
        _get_latest_source()
        _update_virtualenv()
        _create_or_update_dotenv()
        _update_static_files()
        _update_database()
