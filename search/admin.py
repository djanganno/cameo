from django.contrib import admin
from .models import Location, Restaurant, RestDesc

admin.site.register(Location)
admin.site.register(Restaurant)
admin.site.register(RestDesc)
