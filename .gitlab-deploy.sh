#!/bin/bash
set -f
echo "Deploy project on server 35.170.105.153"
ssh ubuntu@35.170.105.153 "mkdir /home/ubuntu/cameo/ && cd /home/ubuntu/cameo/ && git pull origin master -f && bash deploy.sh"
