docker-compose -f production.yml stop
docker-compose -f production.yml pull
docker-compose -f production.yml up -d --remove-orphans
docker ps -q -f status=exited | xargs --no-run-if-empty docker rm
docker-compose -f production.yml run django /app/manage.py migrate --noinput
docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi
docker-compose -f production.yml run django /app/manage.py collectstatic --noinput
